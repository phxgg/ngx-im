import { NgModule } from '@angular/core';
import { LayoutComponent } from './layout/layout.component';
import { RouterModule, Routes } from '@angular/router';
import { ContentComponent } from './components/content/content.component';
import { EmptyContentComponent } from './components/empty-content/empty-content.component';
import { NgxImComponent } from './ngx-im.component';

const routes: Routes = [
{
  path: '',
  component: NgxImComponent,
  children:   [{
    path: '',
    component: LayoutComponent,
    children: [
      {
        path: '', 
        component: EmptyContentComponent
      },
      {
        path: 'room',
        component: EmptyContentComponent
      },
      {
        path: 'room/:id',
        component: ContentComponent
      }
    ]
    }]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class NgxImRoutingModule { }

import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { IChannel } from '../interfaces/IChannel';
import { IMessage } from '../interfaces/IMessage';
import { IAccount } from '../interfaces/IAccount';
import { IConfig } from '../interfaces/IConfig';

@Injectable({
  providedIn: 'root'
})
export class DataService {

    private readonly _openSidebar = new BehaviorSubject<boolean>(true);
    readonly _openSidebar$ = this._openSidebar.asObservable();
  
    private readonly _selectedChannel = new BehaviorSubject<IChannel>(null);
    readonly _selectedChannel$ = this._selectedChannel.asObservable();

    private readonly _channels = new BehaviorSubject<IChannel[]>([]);
    readonly _channels$ = this._channels.asObservable();

    private readonly _messages = new BehaviorSubject<IMessage[]>([]);
    readonly _messages$ = this._messages.asObservable();

    private readonly _replies = new BehaviorSubject<IMessage[]>([]);
    readonly _replies$ = this._replies.asObservable();

    private readonly _error = new BehaviorSubject<string>(null);
    readonly _error$ = this._error.asObservable();

    private readonly _scrollToBottom = new BehaviorSubject<boolean>(true);
    readonly _scrollToBottom$ = this._scrollToBottom.asObservable();

    private readonly _scrollToBottomThread = new BehaviorSubject<boolean>(true);
    readonly _scrollToBottomThread$ = this._scrollToBottomThread.asObservable();

    private readonly _messagesCountChannel = new BehaviorSubject<number>(0);
    readonly _messagesCountChannel$ = this._messagesCountChannel.asObservable();

    private readonly _totalRepliesMessage = new BehaviorSubject<number>(0);
    readonly _totalRepliesMessage$ = this._totalRepliesMessage.asObservable();

    private readonly _account = new BehaviorSubject<IAccount>(null);
    readonly _account$ = this._account.asObservable();

    private _config: IConfig = null;
  
    constructor() {}
  
    get openSidebar(): boolean {
        return this._openSidebar.getValue();
    }
  
    set openSidebar(val: boolean) {
        this._openSidebar.next(!val);
    }
  
    get selectedChannel(): IChannel {
      return this._selectedChannel.getValue();
    }
  
    set selectedChannel(val: IChannel) {
        this._selectedChannel.next(val);
    }

    get channels(): IChannel[] {
        return this._channels.getValue();
    }
    
    set channels(channels: IChannel[]) {
        this._channels.next(channels);
    }

    get messages(): IMessage[] {
        return this._messages.getValue();
    }
    
    set messages(messages: IMessage[]) {
        this._messages.next(messages);
    }

    get error(): string {
        return this._error.getValue();
    }
    
    set error(val: string) {
        this._error.next(val);
    }

    get scrollToBottom(): boolean {
        return this._scrollToBottom.getValue();
    }
    
    set scrollToBottom(val: boolean) {
        this._scrollToBottom.next(val);
    }

    get scrollToBottomThread(): boolean {
        return this._scrollToBottomThread.getValue();
    }
    
    set scrollToBottomThread(val: boolean) {
        this._scrollToBottomThread.next(val);
    }

    get replies(): IMessage[] {
        return this._replies.getValue();
    }
    
    set replies(messages: IMessage[]) {
        this._replies.next(messages);
    }

    get messagesCountChannel(): number {
        return this._messagesCountChannel.getValue();
    }
    
    set messagesCountChannel(val: number) {
        this._messagesCountChannel.next(val);
    }

    get totalRepliesMessage(): number {
        return this._totalRepliesMessage.getValue();
    }
    
    set totalRepliesMessage(val: number) {
        this._totalRepliesMessage.next(val);
    }

    get account(): IAccount {
        return this._account.getValue();
    }
    
    set account(val: IAccount) {
        this._account.next(val);
    }

    get config(): IConfig {
        return this._config;
    }
    
    set config(val: IConfig) {
        this._config = Object.assign({}, val)
    }
}

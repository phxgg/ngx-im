import { InjectionToken, ModuleWithProviders, NgModule } from '@angular/core';
import { LayoutComponent } from './layout/layout.component';
import { NgxImRoutingModule } from './ngx-im-routing.module';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { ContentActionsComponent } from './components/content/content-actions/content-actions.component';
import { ContentComponent } from './components/content/content.component';
import { CommonModule } from '@angular/common';
import { EmptyContentComponent } from './components/empty-content/empty-content.component';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { AppHeaderModule, AppSidebarModule } from '@coreui/angular';
import { HeaderComponent } from './components/header/header.component';
import {DATA_CONTEXT_CONFIG, AngularDataContext } from '@themost/angular';
import { PaginationModule } from 'ngx-bootstrap/pagination';
import { PERFECT_SCROLLBAR_CONFIG } from 'ngx-perfect-scrollbar';
import { PerfectScrollbarConfigInterface } from 'ngx-perfect-scrollbar';
import { MembersChannelComponent } from './components/modals/members-channel/members-channel.component';
import { HideChannelComponent } from './components/modals/hide-channel/hide-channel.component';
import { ModalModule } from 'ngx-bootstrap/modal';
import { AddEditMemberChannelComponent } from './components/modals/add-edit-member-channel/add-edit-member-channel.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PaginationPipePipe } from './pipes/pagination-pipe.pipe';
import { NewMembersTableComponent } from './components/modals/components/new-members-table/new-members-table.component';
import { CurrentMembersTableComponent } from './components/modals/components/current-members-table/current-members-table.component';
import { ContentMessagesComponent } from './components/content/content-messages/content-messages.component';
import { QuillModule } from 'ngx-quill';
import { NgxImComponent } from './ngx-im.component';
import { AddChannelComponent } from './components/modals/add-channel/add-channel.component';
import { EditChannelComponent } from './components/modals/edit-channel/edit-channel.component';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { ContentThreadComponent } from './components/modals/content-thread/content-thread.component';
import { AlertModule } from 'ngx-bootstrap/alert';
import { ErrorComponent } from './components/error/error.component';
import { NoSanitizePipe } from './pipes/noSanitize-pipe.pipe';
import { IConfig } from './interfaces/IConfig';
import { APP_CONFIG } from './ngx-im.config';

const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
  suppressScrollX: true
};

@NgModule({
  declarations: [
    NgxImComponent,
    LayoutComponent,
    SidebarComponent,
    ContentComponent,
    ContentActionsComponent,
    EmptyContentComponent,
    HeaderComponent,
    MembersChannelComponent,
    HideChannelComponent,
    AddEditMemberChannelComponent,
    PaginationPipePipe,
    NoSanitizePipe,
    NewMembersTableComponent,
    CurrentMembersTableComponent,
    ContentMessagesComponent,
    AddChannelComponent,
    EditChannelComponent,
    ContentThreadComponent,
    ErrorComponent
  ],
  imports: [
    CommonModule,
    FormsModule,   
    AppHeaderModule,
    AppSidebarModule,
    PerfectScrollbarModule,
    NgxImRoutingModule,
    ReactiveFormsModule,
    AlertModule.forRoot(),
    BsDropdownModule.forRoot(),
    PaginationModule.forRoot(),
    ModalModule.forRoot(),
    QuillModule.forRoot(),
    TooltipModule.forRoot()
  ],
  providers: [
    {
      provide: DATA_CONTEXT_CONFIG, useValue: {
          base: 'http://localhost:5001/api/',
          options: {
              useMediaTypeExtensions: false,
              useResponseConversion: true,
          },
      },
  },
  {
    provide: PERFECT_SCROLLBAR_CONFIG,
    useValue: DEFAULT_PERFECT_SCROLLBAR_CONFIG
  },
    AngularDataContext
  ],
  exports: []
})

export class NgxImModule { 

  static forRoot(conf?: IConfig): ModuleWithProviders<NgxImModule> {
    return {
      ngModule: NgxImModule,
      providers: [ {provide: APP_CONFIG, useValue: conf} ]
    }
}
}

export interface IMember {
    account: number; 
    additionalType:string; 
    createdBy: number; 
    dateCreated: Date; 
    dateModified: Date; 
    name: string, 
    serviceProvider: string, 
    id: number, 
    modifiedBy: number;
    class?: string;
    $state?: number;
}
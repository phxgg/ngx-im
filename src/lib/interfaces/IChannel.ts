import { IMember } from "./IMember";
import { INewUser } from "./INewUser";

export interface IChannel {
    alternateName: string;
    dateCreated: Date;
    dateModified: Date;
    expiresAt: string;
    headline: string;
    maintainer: IMember;
    thumbnailUrl: string;
    keywords: string[];
    isDirect: number | boolean;
    id: number;
    members: IMember[];
    type: 1 | 2;
}

export interface IChannelPost {
    alternateName: string;
    headline: string;
    thumbnailUrl: string;
    keywords: string[];
    members: INewUser[];
    isDirect?: boolean;
}


export interface IChannelHide {
    id: number;
    isDisabled: boolean;
}
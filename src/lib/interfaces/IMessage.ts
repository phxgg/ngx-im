import { IMember } from "./IMember";
import { IReaction } from "./IReaction";

export interface IMessage {
    additionalType: string;
    body: string;
    createdBy: IMember;
    dateCreated: string;
    dateModified: string;
    id: number;
    identifier: string;
    isReply: boolean;
    parentInstantMessage: number;
    hasReply: boolean;
    modifiedBy: number;
    owner: number;
    reactions: IReaction[];
    recipient: number;
    sender: IMember;
    accountType: number;
    description: string;
    from: string;
    likes?: IMember[];
    super?: IMember[];
    liked?: boolean;
    supered?: boolean;
    hasName?: boolean;
    extraPadding?: boolean;
}
export interface IMessagePost {
    body: string;
    isReply: boolean;
    hasReply: boolean;
    parentInstantMessage?: number;
}
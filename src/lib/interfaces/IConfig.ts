export interface IConfig {
    MIN_HEIGHT_IM: string;
    MAX_HEIGHT_IM: string;
    MAX_WIDTH_MESSAGES: string;
    MAX_HEIGHT_REPLIES: string;
}


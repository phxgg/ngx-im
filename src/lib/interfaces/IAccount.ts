import { IMember } from "./IMember";

export interface IGroup {
    alternateName: string; 
    id: number; 
    name: string;
}

export interface IAccount{
    accountType: number
    additionalType: string;
    createdBy: number;
    dateCreated: Date;
    dateModified: Date;
    description: string;
    enabled: number;
    groups: IGroup[];
    id: number;
    imAccount: IMember;
    logonCount: number;
    modifiedBy: number
    name: string;
}
import { Component, Inject, OnInit, Optional } from '@angular/core';
import { AngularDataContext } from '@themost/angular';
import { IConfig } from './interfaces/IConfig';
import { DataService } from './services/data.service';
import { APP_CONFIG } from './ngx-im.config';


const MIN_HEIGHT_IM_DEFAULT = 'unset'; // px
const MAX_HEIGHT_IM_DEFAULT  = '100%'; // px
const MAX_WIDTH_MESSAGES_DEFAULT  = '40%'; // percent
const MAX_HEIGHT_REPLIES_DEFAULT  = '50vh'; // viewport percentage (vh)
@Component({
  selector: 'app-instant-messages',
  template: `<router-outlet></router-outlet>`,
  styles: []
})
export class NgxImComponent implements OnInit {

  constructor(private context: AngularDataContext, public dataService: DataService, @Optional() @Inject(APP_CONFIG) config: IConfig) {
    const { token } = JSON.parse(localStorage.getItem('currentUser') || sessionStorage.getItem('currentUser')) || { token: { access_token: '' }}
    const { access_token } = token;
    this.context.setBearerAuthorization(access_token);
    const conf: IConfig = {
      MIN_HEIGHT_IM: config.MIN_HEIGHT_IM || MIN_HEIGHT_IM_DEFAULT,
      MAX_HEIGHT_IM: config.MAX_HEIGHT_IM || MAX_HEIGHT_IM_DEFAULT,
      MAX_WIDTH_MESSAGES: config.MAX_WIDTH_MESSAGES || MAX_WIDTH_MESSAGES_DEFAULT,
      MAX_HEIGHT_REPLIES: config.MAX_HEIGHT_REPLIES || MAX_HEIGHT_REPLIES_DEFAULT,
    }
    this.dataService.config = conf;
  }

  ngOnInit(): void {}

}

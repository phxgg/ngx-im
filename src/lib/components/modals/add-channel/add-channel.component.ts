import { Component, EventEmitter, OnDestroy, OnInit, Output, ViewChild } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, ValidationErrors, ValidatorFn, Validators } from '@angular/forms';
import { AngularDataContext } from '@themost/angular';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { IChannel, IChannelPost } from '../../../interfaces/IChannel';
import { IMember } from '../../../interfaces/IMember';
import { INewUser } from '../../../interfaces/INewUser';
import { DataService } from '../../../services/data.service';
import { AddEditMemberChannelComponent } from '../add-edit-member-channel/add-edit-member-channel.component';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { IAccount } from '../../../interfaces/IAccount';

const MAX_PAGINATION_SIZE = 5;

@Component({
  selector: 'im-add-channel',
  templateUrl: './add-channel.component.html',
  styleUrls: ['./add-channel.component.scss']
})
export class AddChannelComponent implements OnInit, OnDestroy {
  @ViewChild('addChannelModal') public addChannelModal: ModalDirective;
  @ViewChild(AddEditMemberChannelComponent) addEditMemberChannelModal: AddEditMemberChannelComponent;
  @Output() newChannel = new EventEmitter<IChannel>();

  form: FormGroup;
  submitted = false;
  newMembers: INewUser[] = [];
  maxSize: number = MAX_PAGINATION_SIZE;
  keywords: string[] = [];
  loading: boolean = false;
  type: 'channel' | 'direct' | null;
  user: IMember;
  types = [{label: 'Global', value: 1}, {label: 'Educational', value: 2}];
  private _unsubscribeAll: Subject<any>;
  
  constructor(private formBuilder: FormBuilder, public dataService: DataService, private context:AngularDataContext) {
    this._unsubscribeAll = new Subject();
   }

  ngOnInit(): void {
    this.dataService.
    _account$
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe(res => {
        const data: IAccount = {...res};
        const { imAccount } = data;
        this.user = imAccount;
      });
    this.form = this.formBuilder.group(
      {
        headline: ['', [Validators.required]],
        alternateName: ['', [Validators.required]],
        name: ['', [Validators.required]],
        thumbnailUrl: ['', [this.urlValidator()]],
        type: [this.types[0].value, [Validators.required]],
        keyword: [''],
      },
    );
  }

  onSubmit(): void {
    this.submitted = true;
    if (this.form.invalid) return;
    const { alternateName, headline, thumbnailUrl, name, type } = this.form.value;
    const data = {
      alternateName,
      headline,
      thumbnailUrl,
      type: parseInt(type),
      keywords: this.keywords,
      isDirect: this.type === 'direct',
      members: this.type === 'direct' ?  [{ name: name } , { name: this.user.name }] : (this.newMembers.find(x => x.name === this.user.name) ? [...this.newMembers] : [ ...this.newMembers, {name: this.user.name} ])
    }
    this.saveApiData(data)
  }

  onReset(): void {
    this.submitted = false;
    this.keywords = [];
    this.newMembers = [];
    this.type = null;
    this.form.get('headline').setValue('');
    this.form.get('alternateName').setValue('');
    this.form.get('name').setValue('');
    this.form.get('thumbnailUrl').setValue('');
    this.form.get('keyword').setValue('');
    this.form.get('type').setValue(this.types[0].value)
    this.addChannelModal.hide();
  }

  openAddChannelModal(type: 'channel' | 'direct') {
    if(type === 'direct') {
      this.form.get('name').setValidators([Validators.required])
    } else {
      this.form.get('name').clearValidators()
    }
    this.form.get('name').updateValueAndValidity()
    this.type = type;
    this.addChannelModal.show()
  }

  get f(): { [key: string]: AbstractControl } {
    return this.form.controls;
  }

  newUser(user: INewUser) {
    this.newMembers.push({
      ...user,
    })
    this.newMembers = this.newMembers.slice()
  }

  removeEmit(user: IMember) {
    this.newMembers = this.newMembers.filter(x => x !== user)
  }

  openAddMemberModal() {
    this.addEditMemberChannelModal.openAddMembersModal()
  }

  appendKeyword() {
    const keyword = this.form.get('keyword').value;
    const alreadyIncluded = this.keywords.find(x => x === keyword.trim())
    if(!alreadyIncluded && keyword.trim()) {
      this.keywords.push(keyword.trim());
      this.form.get('keyword').setValue('')
    }
  }

  removeKeyword(keyword: string) {
    this.keywords = this.keywords.filter(x => x !== keyword)
  }

  urlValidator(): ValidatorFn {
    return (control:AbstractControl) : ValidationErrors | null => {
      let validUrl = true;
      const value = control.value;
      try {
        if(value) new URL(value)
      } catch {
        validUrl = false;
      }
  
      return validUrl ? null : { invalidUrl: true };
    }
  }

  saveApiData(data: IChannelPost) {
    this.loading = true;
    this.context
          .model("MessagingChannels")
          .save(data)
          .then(results => {
            const channel: IChannel = {...results, maintainer: this.user};
            this.newChannel.emit(channel);
            this.onReset();
          })
          .catch(error => this.dataService.error = error.message)
          .finally(() => this.loading = false)
  }

  editEmit(user: IMember) {
    this.addEditMemberChannelModal.openEditMembersModal(user)
  }

  editUser(data: {oldUser: IMember; newUser: IMember}) {
    const {oldUser, newUser } = data;
    this.newMembers = this.newMembers.map(x => x.name === oldUser.name ? newUser : x);
  }

  ngOnDestroy(): void {
    this._unsubscribeAll.next();
    this._unsubscribeAll.complete();
  }
  
}

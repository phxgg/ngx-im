import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HideChannelComponent } from './hide-channel.component';

describe('HideChannelComponent', () => {
  let component: HideChannelComponent;
  let fixture: ComponentFixture<HideChannelComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HideChannelComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HideChannelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { AngularDataContext } from '@themost/angular';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { IChannel, IChannelHide } from '../../../interfaces/IChannel';
import { DataService } from '../../../services/data.service';
import { Router } from '@angular/router';

@Component({
  selector: 'im-hide-channel',
  templateUrl: './hide-channel.component.html',
  styleUrls: ['./hide-channel.component.scss']
})
export class HideChannelComponent implements OnInit, OnDestroy {
  @ViewChild('hideChannel') public hideChannel: ModalDirective;
  room: IChannel;
  channels: IChannel[];
  loading: boolean = false;
  private _unsubscribeAll: Subject<any>;
  constructor(private context:AngularDataContext, private dataService: DataService, private router: Router) { 
    this._unsubscribeAll = new Subject();
  }

  ngOnInit(): void {
    this.dataService.
    _selectedChannel$
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe(val => {
        if(val) {
          this.room = val;
        }
      })
      this.dataService.
        _channels$
        .pipe(takeUntil(this._unsubscribeAll))
        .subscribe(val => {
          if(val) {
            this.channels = val;
          }
        })
  }

  openHideModal() {
    this.hideChannel.show()
  }

  hide() {
    const { id } = this.room;
    const data = {
      id,
      isDisabled: true
    }
    this.hideApiData(data)
  }

  hideApiData(data: IChannelHide) {
    this.loading = true;
    this.context
          .model("MessagingChannels")
          .save(data)
          .then(() => {
            const { id } = data;
            const url = this.router.url;
            this.dataService.selectedChannel = null;
            this.dataService.channels = [...this.channels.filter(x => x.id !== id)];
            this.router.navigate(url.split('/').slice(0, -1))
                       .finally(() => this.hideChannel.hide())
          })
          .catch(error => this.dataService.error = error.message)
          .finally(() => this.loading = false);
  }

  ngOnDestroy(): void {
    this._unsubscribeAll.next();
    this._unsubscribeAll.complete();
  }
}

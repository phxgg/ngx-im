import { Component, EventEmitter, Input, OnChanges, OnDestroy, OnInit, Output, SimpleChanges } from '@angular/core';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { IMember } from '../../../../interfaces/IMember';
import { IAccount } from '../../../../interfaces/IAccount';
import { DataService } from '../../../../services/data.service';
@Component({
  selector: 'im-current-members-table',
  templateUrl: './current-members-table.component.html',
  styleUrls: ['./current-members-table.component.scss']
})
export class CurrentMembersTableComponent implements OnInit, OnChanges, OnDestroy {

  @Input() members: IMember[];
  @Input() maxSize: number;
  @Output() removeEmit = new EventEmitter<IMember>();
  currentUser: IMember;
  private _unsubscribeAll: Subject<any>;
  
  constructor(private dataService: DataService) {
    this._unsubscribeAll = new Subject();
   }

  currentPage: number = 1;
  totalItems: number = 0;

  ngOnInit(): void {
    this.dataService.
    _account$
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe(res => {
        const data: IAccount = {...res};
        const { imAccount } = data;
        this.currentUser = imAccount;
      });
  }

  ngOnChanges(changes: SimpleChanges) {
    if(changes.members && changes.members.currentValue) {
      if(this.members.length > 0 && this.currentPage > 1 && !this.members.slice((this.currentPage - 1) * this.maxSize, this.currentPage * this.maxSize).length) {
        this.currentPage = this.currentPage - 1;
      }
      this.totalItems = this.members.length;
    }
  }

  pageChanged(event:any) {
    this.currentPage = event.page;
  }

 remove(user: IMember) {
  this.removeEmit.emit(user);
 }

 ngOnDestroy(): void {
  this._unsubscribeAll.next();
  this._unsubscribeAll.complete();
}

}

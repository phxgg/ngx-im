import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';
import { IMember } from '../../../../interfaces/IMember';


@Component({
  selector: 'im-new-members-table',
  templateUrl: './new-members-table.component.html',
  styleUrls: ['./new-members-table.component.scss']
})
export class NewMembersTableComponent implements OnInit, OnChanges {

  @Input() members: IMember[];
  @Input() maxSize: number;
  @Output() removeEmit = new EventEmitter<IMember>();
  @Output() editEmit = new EventEmitter<IMember>();
  // New members section
  currentPage: number = 1;
  totalItems: number = 0;

  constructor() {}

  ngOnInit(): void {}

  ngOnChanges(changes: SimpleChanges) {
    if(changes.members && changes.members.currentValue) {
      if(this.members.length > 0 && this.currentPage > 1 && !this.members.slice((this.currentPage - 1) * this.maxSize, this.currentPage * this.maxSize).length) {
        this.currentPage = this.currentPage - 1;
      }
      this.totalItems = this.members.length;
    }
  }

  pageChanged(event:any) {
    this.currentPage = event.page;
 }

 remove(user: IMember) {
  this.removeEmit.emit(user)
 }

 edit(user: IMember) {
  this.editEmit.emit(user)
 }

}

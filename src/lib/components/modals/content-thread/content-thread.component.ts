import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { AngularDataContext } from '@themost/angular';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { IChannel } from '../../../interfaces/IChannel';
import { IMember } from '../../../interfaces/IMember';
import { IMessage } from '../../../interfaces/IMessage';
import { IReaction, IReactionPost } from '../../../interfaces/IReaction';
import { DataService } from '../../../services/data.service';
import { IAccount } from '../../../interfaces/IAccount';

const PAGE = 20;
@Component({
  selector: 'im-content-thread',
  templateUrl: './content-thread.component.html',
  styleUrls: ['./content-thread.component.scss']
})
export class ContentThreadComponent implements OnInit, OnDestroy {
  user: IMember;
  message: IMessage;
  messages: IMessage[] = [];
  messagesChannel: IMessage[] = [];
  index: number = 0;
  disabled: boolean = false;
  channel: IChannel;
  loadingMessages: boolean = false;
  loadingInitMessages: boolean = false;
  totalRepliesMessages: number = 0;
  private _unsubscribeAll: Subject<any>;
  @ViewChild('threadMessages') public threadMessages: ModalDirective;

  constructor(private context: AngularDataContext, public dataService: DataService) { 
    this._unsubscribeAll = new Subject();
  }

  ngOnInit(): void {
    this.dataService.
    _account$
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe(res => {
        const data: IAccount = {...res};
        const { imAccount } = data;
        this.user = imAccount;
      });

    this.dataService.
      _selectedChannel$
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe(val => {
        if(val) {
          this.channel = val;
        }
    })
    this.dataService.
      _replies$
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe(val => {
        this.messages = val || [];
      });
    this.dataService.
      _messages$
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe(val => {
        this.messagesChannel = val || [];
      });
      this.dataService.
        _totalRepliesMessage$
        .pipe(takeUntil(this._unsubscribeAll))
        .subscribe(res => {
          this.totalRepliesMessages = res;
        });
  }

  openThreadMessagesModal(message: IMessage) {
    this.message = message;
    this.dataService.scrollToBottomThread = true;
    this.index = 0;
    this.fetchMessageThread()
    this.threadMessages.show()
  }

  fetchMessageThread() {
    if(this.index === 0) this.loadingInitMessages = true;
    this.loadingMessages = true;
    this.context
    .model(`MessagingChannels/${this.channel.id}/messages`)
    .where("parentInstantMessage")
    .equal(this.message.id)
    .expand("createdBy", "sender", "reactions", "reactions($expand=user)")
    .take(PAGE)
    .skip(PAGE * this.index)
    .orderByDescending("dateCreated")
    .getList()
    .then(results => {
      const { value, total } = results;
      if(this.index === 0) {
        this.dataService.totalRepliesMessage = total;
        this.dataService.replies = [];
      }
      this.dataService.replies = [...this.messages, ...value]
      this.disabled = value.length < PAGE;
      this.index++;
    })
    .catch(error => {
      this.disabled = true;
      this.dataService.error = error.message
    })
    .finally(() => {
      this.loadingInitMessages = false;
      this.loadingMessages = false
    });
  }

  fetchMore() {
    if(!this.disabled) this.fetchMessageThread()
  }

  reactionAction(event: {message: IMessage, index: number}) {
    const { message, index } = event;
    const reaction: IReactionPost = {
      reaction: index,
      instantMessage: message.id
    }
    const tempDisabled = this.disabled;
    this.disabled = true;
    this.context
    .model(`MessagingChannels/${this.channel.id}/toggleReaction`)
    .save(reaction)
    .then((results: IReaction) => {
        const newMessage: IMessage = 
          results ? {
            ...message,
            reactions: [
              ...message.reactions, 
              {
              ...results,
              user: this.user
              }
            ].sort((x,y) => x.id - y.id)
          } : {
            ...message,
            reactions: message.reactions.filter(x => x.reaction !== index)
          }
        const newMessages = this.messages.map(x => x.id === newMessage.id ? newMessage : x);
        this.dataService.replies = [...newMessages];
    })
    .catch(error => this.dataService.error = error.message)
    .finally(() => this.disabled = tempDisabled);
  }

  deleteMessage(event: {message: IMessage}) {
    const { message } = event;
    const tempDisabled = this.disabled;
    this.disabled = true;
    this.context
    .model(`MessagingChannels/${this.channel.id}/removeMessage`)
    .save(message)
    .then((results: {item: IMessage; children: IMessage}) => {
      const { item } = results;
      const data = [...this.messages].filter(x => x.id !== item.id)
      this.dataService.replies = [...data];
      this.dataService.totalRepliesMessage = this.totalRepliesMessages - 1;
      if(!data.length) this.updateMessageReply()
    })
    .finally(() => this.disabled = tempDisabled);
  }

  updateMessageReply() {
    this.context
    .model(`MessagingChannels/${this.channel.id}/toggleMessageReply`)
    .save({
      ...this.message,
    })
    .then(message => {
      const index = this.messagesChannel.findIndex(x => x.id === message.id)
      if(index > -1) {
        this.message.hasReply = message.hasReply;
        this.messagesChannel[index].hasReply = message.hasReply;
        this.dataService.messages = [...this.messagesChannel];
      }
    })
  }
  styleObject = (): Object => ({minHeight: `${this.dataService.config.MIN_HEIGHT_IM}`, maxHeight: `${this.dataService.config.MAX_HEIGHT_REPLIES}`})

  ngOnDestroy(): void {
    this._unsubscribeAll.next();
    this._unsubscribeAll.complete();
  }
}

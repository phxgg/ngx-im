import { ChangeDetectorRef, Component, EventEmitter, OnDestroy, OnInit, Output, ViewChild, ViewEncapsulation } from '@angular/core';
import { AngularDataContext } from '@themost/angular';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { IChannel } from '../../../interfaces/IChannel';
import { IMember } from '../../../interfaces/IMember';
import { INewUser } from '../../../interfaces/INewUser';
import { DataService } from '../../../services/data.service';
import { AddEditMemberChannelComponent } from '../add-edit-member-channel/add-edit-member-channel.component';
import { uniqBy } from 'lodash';
import { IAccount } from '../../../interfaces/IAccount';

const MAX_PAGINATION_SIZE = 5;
const DELETE_CODE = 4;
@Component({
  selector: 'im-members-channel',
  templateUrl: './members-channel.component.html',
  styleUrls: ['./members-channel.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class MembersChannelComponent implements OnInit, OnDestroy {
  @ViewChild('membersModal') public membersModal: ModalDirective;
  @ViewChild(AddEditMemberChannelComponent) addEditMemberChannelModal: AddEditMemberChannelComponent;
  @Output() editNewChannel = new EventEmitter<{oldChannel: IChannel; newChannel: IChannel}>();

  room: IChannel;
  channelTableMembers: IMember[] = [];
  channelMembers: IMember[] = [];
  newMembers: INewUser[] = [];
  editMember: IMember;
  maxSize: number = MAX_PAGINATION_SIZE;
  loading: boolean = false;
  user: IMember;
  private _unsubscribeAll: Subject<any>;

  constructor(private context:AngularDataContext, private dataService: DataService) {
    this._unsubscribeAll = new Subject();
  }

  ngOnInit(): void {
    this.dataService.
    _account$
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe(res => {
        const data: IAccount = {...res};
        const { imAccount } = data;
        this.user = imAccount;
      });

    this.dataService.
    _selectedChannel$
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe(val => {
        if(val) {
          this.room = val;
          const members = val.members || [];
          this.getData(members);
        }
      })
  }

  getData(members: IMember[]) {
    this.channelMembers = members.map(x => ({
      ...x,
      class: this.getBadgeClass(x.additionalType.toLowerCase())
    }));
    this.channelTableMembers = [...this.channelMembers];
    this.channelTableMembers = this.channelTableMembers.slice();
  }

  getBadgeClass(additionalType: string) {
    switch(additionalType) {
      case 'user':
        return 'badge badge-success'
      case 'account':
          return 'badge badge-secondary'
      default:
        return ''
    }
  }
  
  openEditModal() {
    this.channelTableMembers = [...this.channelMembers];
    this.newMembers = [];
    this.membersModal.show()
  }

  openAddMemberModal() {
    this.addEditMemberChannelModal.openAddMembersModal()
  }

  newUser(user: INewUser) {
    this.newMembers.push({
      ...user,
    })
    this.newMembers = this.newMembers.slice()
  }

  isDisabled() {
    const totalUsers = this.channelTableMembers.length + this.newMembers.length;
    return totalUsers === 0 || totalUsers === 1;
  }

  close() {
    this.membersModal.hide();
  }

  save() {
    const members = 
      uniqBy([
        ...(this.channelMembers.map(({ ['class']: _, ...keep }) => keep)
                               .map(x => this.channelTableMembers.find(y => y.id === x.id) || this.newMembers.find(z => z.name === x.name)
                                  ? x : {...x, $state: DELETE_CODE})
                                ),
        ...this.newMembers       
      ], 'name');
    const savedMembers = members.find(x => x.name === this.user.name) ? [...members] : [ ...members, {name: this.user.name} ];
    this.saveApiData(savedMembers);
  }

  removeEmit(user: IMember, type: 'current' | 'new') {
    switch(type) {
      case 'current':
        this.channelTableMembers = this.channelTableMembers.filter(x => x !== user)
        return;
      case 'new':
        this.newMembers = this.newMembers.filter(x => x !== user)
        return;
      default:
        return;
    }

  }

  async saveApiData(data: INewUser[]) {
    this.loading = true;
    try {
      const count = data.filter(y => !y.$state).length;
      const channel: IChannel = await this.editChannelType(count === 2);
      const members: IMember[] = await this.saveChannelMembers(data);
      const newMembers = members
                        .filter(y => !y.$state)
                        .map(x => ({
                          ...x,
                          additionalType: x.additionalType ?? 'Account',
                        }))
      this.saveChannel({
        ...channel,
        members: newMembers
      });
      this.newMembers = [];
      this.close();
    }
    catch (err) {
      this.dataService.error = err.message
    }
    finally {
      this.loading = false
    }
  }

  editChannelType(isDirect: boolean) {
    const data = {
      ...this.room,
      isDirect
    }
    return this.context
          .model(`MessagingChannels`)
          .save(data)
  }

  saveChannel(channel: IChannel) {
    this.editNewChannel.emit({oldChannel: this.room, newChannel: channel});
  }

  async saveChannelMembers(data: INewUser[]) {
    const result = await this.context
      .model(`MessagingChannels/${this.room.id}/Members`)
      .save(data);
    return result;
  }

  editEmit(user: IMember) {
    this.addEditMemberChannelModal.openEditMembersModal(user)
  }

  editUser(data: {oldUser: IMember; newUser: IMember}) {
    const {oldUser, newUser } = data;
    this.newMembers = this.newMembers.map(x => x.name === oldUser.name ? newUser : x);
  }

  ngOnDestroy(): void {
    this._unsubscribeAll.next();
    this._unsubscribeAll.complete();
  }
  
}

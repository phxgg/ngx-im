import { Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { IChannel } from '../../interfaces/IChannel';
import { ISidebarItemNav, ISidebarNav } from '../../interfaces/ISidebarNav';
import { DataService } from '../../services/data.service';
import { isEmpty, find } from 'lodash';
import { IAccount, IGroup } from '../../interfaces/IAccount';
@Component({
  selector: 'im-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss'],
})
export class SidebarComponent implements OnInit, OnDestroy {
  @Output() openAddChannelModal = new EventEmitter<'channel' | 'direct'>();
  @Input() loading: boolean;
  channels: IChannel[];
  menusInit: ISidebarNav[] = [];
  menus: ISidebarNav[] = [];
  searchValue: string = '';
  init: boolean = true;
  private _unsubscribeAll: Subject<any>;
  isAdmin: boolean = false;
  
  constructor(public dataService: DataService) {
    this._unsubscribeAll = new Subject();
  }

  ngOnInit() {
    this.dataService.
    _account$
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe(res => {
        const data: IAccount = {...res};
        this.isAdmin = !isEmpty(find(data.groups, (x: IGroup) => x.name === 'Administrators'))
      });
    this.dataService.
      _channels$
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe(val => {
        const channels = val || [];
        this.menusInit = 
        [{
          title: 'Channels',
          type: 'channel',
          collapse: this.menus[0]?.collapse ? this.menus[0].collapse : this.init,
          children: channels
                          .filter(x => !x.isDirect)
                          .map(channel => 
                            ({
                              headline: channel.headline,
                              keywords: channel.keywords,
                              alternateName: channel.alternateName,
                              id: channel.id,
                            }))
        },  
        {
          title: 'Direct Messages',
          type: 'direct',
          collapse: this.menus[1]?.collapse ? this.menus[1].collapse : false,
          children: channels
                          .filter(x => x.isDirect)
                          .map(channel => 
                            ({
                              headline: channel.headline,
                              keywords: channel.keywords,
                              alternateName: channel.alternateName,
                              id: channel.id,
                            }))
        }]
        this.onSearchChange(this.searchValue);
      })
  }

  checkTitleAndChannelKeywords(sideNavItem: ISidebarItemNav) {
    const keywords = sideNavItem.keywords || [];
    const title = sideNavItem.headline || "";
    const alternateName = sideNavItem.alternateName || "";
    const isIncluded = keywords.find(x => x.toLowerCase().indexOf(this.searchValue) > -1) || title.toLowerCase().indexOf(this.searchValue) > -1 || alternateName.toLowerCase().indexOf(this.searchValue) > -1
    return isIncluded;
  }
  
  onSearchChange(searchValue: string) {
    this.searchValue = searchValue.toLowerCase().trim();
    this.menus = this
                  .menusInit
                  .map(x => ({
                    ...x,
                    children: x.children.filter(y => this.checkTitleAndChannelKeywords(y))
                  }))
    this.init = false;
  }

  collapseMenus(index: number) {
    this.menus[index].collapse = !this.menus[index].collapse
  }
  openAddChannel(index: number) {
    const menu = this.menus[index];
    const { type } = menu
    this.openAddChannelModal.emit(type)
  }

  ngOnDestroy(): void {
    this._unsubscribeAll.next();
    this._unsubscribeAll.complete();
  }
}

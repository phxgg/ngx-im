import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { IChannel } from '../../interfaces/IChannel';
import { MembersChannelComponent } from '../modals/members-channel/members-channel.component';
import { HideChannelComponent } from '../modals/hide-channel/hide-channel.component';
import { EditChannelComponent } from '../modals/edit-channel/edit-channel.component';
import { DataService } from '../../services/data.service';
import _ from 'lodash';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { IMember } from '../../interfaces/IMember';
import { IAccount } from '../../interfaces/IAccount';

@Component({
  selector: 'im-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit, OnDestroy {

  @ViewChild(MembersChannelComponent) membersModal:MembersChannelComponent;
  @ViewChild(EditChannelComponent) editModal:EditChannelComponent;
  @ViewChild(HideChannelComponent) hideModal:HideChannelComponent;

  private sideBarOpened: boolean;
  public selectedRoom: IChannel;
  public channels: IChannel[];
  private _unsubscribeAll: Subject<any>;
  currentUser: IMember;
  types = [{label: 'Global', value: 1}, {label: 'Educational', value: 2}];
  constructor(public dataService: DataService) {
    this._unsubscribeAll = new Subject();
  }

  ngOnInit(): void {
    this.dataService.
      _account$
        .pipe(takeUntil(this._unsubscribeAll))
        .subscribe(res => {
          const data: IAccount = {...res};
          const { imAccount } = data;
          this.currentUser = imAccount;
        });
    this.dataService.
        _openSidebar$
        .pipe(takeUntil(this._unsubscribeAll))
        .subscribe(val => this.sideBarOpened = val)
    this.dataService.
        _selectedChannel$
        .pipe(takeUntil(this._unsubscribeAll))
        .subscribe(val => {
          this.selectedRoom = val ? Object.assign({}, {...val, members: val.members.sort((x,_) => x.name === this.currentUser.name ? -1 : 1)}, null) : null;
        })
    this.dataService.
        _channels$
        .pipe(takeUntil(this._unsubscribeAll))
        .subscribe(val => this.channels = val)
  }

  onToggle() {
    this.dataService.openSidebar = this.sideBarOpened;
  }

  membersChannel() {
    this.membersModal.openEditModal()
  }

  hideChannel() {
    this.hideModal.openHideModal()
  }

  editChannel() {
    const type = this.selectedRoom.isDirect ? 'direct' : 'channel';
    const obj: IChannel = _.cloneDeep(this.selectedRoom)
    this.editModal.openEditChannelModal(type, obj)
  }

  editNewChannel(data: {oldChannel: IChannel; newChannel: IChannel}) {
    const { oldChannel, newChannel } = data;
    this.dataService.channels = 
      this.channels.map((x) => x.id === oldChannel.id ? {
        ...newChannel,
        isDirect: newChannel.members.length === 2
      } : x)
      this.dataService.selectedChannel = newChannel;
  }

  getTooltip() {
    return `${this.selectedRoom.alternateName}, ${this.types.find(x => x.value === this.selectedRoom.type).label}`
  }

  ngOnDestroy(): void {
    this._unsubscribeAll.next();
    this._unsubscribeAll.complete();
  }
    
}

import { ChangeDetectorRef, Component, ElementRef, EventEmitter, Input, OnDestroy, OnInit, Output, ViewChild, ViewEncapsulation } from '@angular/core';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { IMember } from '../../../interfaces/IMember';
import { IMessage } from '../../../interfaces/IMessage';
import { ReactionType } from '../../../interfaces/IReaction';
import { DataService } from '../../../services/data.service';
import { ContentThreadComponent } from '../../modals/content-thread/content-thread.component';
import { AngularDataContext } from '@themost/angular';
import { IAccount } from '../../../interfaces/IAccount';

const UPDATE_THRESHOLD = 5;
@Component({
  selector: 'im-content-messages',
  templateUrl: './content-messages.component.html',
  styleUrls: ['./content-messages.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ContentMessagesComponent implements OnInit, OnDestroy {
  messages: IMessage[] = [];
  user: IMember;
  isScrollOnBottomInit: boolean = false;
  position: number = 0;
  ReactionType = ReactionType;
  @Input() isReplyChat: boolean = false;
  @Input() disabled: boolean;
  @Input() isLoading: boolean;
  @Output() fetchMore = new EventEmitter();
  @Output() deleteMessage = new EventEmitter();
  @Output() toggleReaction = new EventEmitter<{message: IMessage, index: number}>();
  @ViewChild(ContentThreadComponent) contentThreadModal:ContentThreadComponent;
  @ViewChild('chatScroll', { static: false }) chatScroll: ElementRef;
  scrollToBottom: boolean = false;
  scrollToBottomThread: boolean = false;
  private _unsubscribeAll: Subject<any>;
  
  constructor(public dataService: DataService, private cd: ChangeDetectorRef, private context: AngularDataContext) {
    this._unsubscribeAll = new Subject();
   }

  ngOnInit(): void {
    this.dataService.
    _account$
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe(res => {
        const data: IAccount = {...res};
        const { imAccount } = data;
        this.user = imAccount;
      });
    if(!this.isReplyChat) this.dataService.scrollToBottom = true;
    this.dataService.
      _scrollToBottom$
        .pipe(takeUntil(this._unsubscribeAll))
        .subscribe(val => this.scrollToBottom = val);
    this.dataService.
      _scrollToBottomThread$
        .pipe(takeUntil(this._unsubscribeAll))
        .subscribe(val => this.scrollToBottomThread = val);
    if(this.isReplyChat) {
      this.dataService.
        _replies$
          .pipe(takeUntil(this._unsubscribeAll))
          .subscribe(val => {
              this.messages = [...this.getRefactorMessages([...this.getExtraFields(val)])];
              if(this.scrollToBottomThread) {
                this.dataService.scrollToBottomThread = false;
                this.cd.detectChanges();
                this.chatScroll.nativeElement.scrollTop = this.chatScroll.nativeElement.scrollHeight;
              }
          });
    } else {
      this.dataService.
      _messages$
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe(val => {
        this.messages = [...this.getRefactorMessages([...this.getExtraFields(val)])];
        if(this.scrollToBottom) {
          this.dataService.scrollToBottom = false;
          this.cd.detectChanges();
          this.chatScroll.nativeElement.scrollTop = this.chatScroll.nativeElement.scrollHeight;
        }
      });
    }
  }

  getRefactorMessages(messages: IMessage[]) {
    const refactorMessages: IMessage[] = []
    for (let i = 0; i < messages.length; i++) {
      const message = messages[i];
      i === 0 ? 
        refactorMessages.push(({
          ...message,
          hasName: true,
          extraPadding: false
        })) 
      : 
        refactorMessages.push(({
          ...message,
          hasName: (new Date(message.dateCreated).getTime() - new Date(messages[i-1].dateCreated).getTime()) / 1000 > UPDATE_THRESHOLD * 60 || messages[i-1].from !== message.from,
          extraPadding: (new Date(message.dateCreated).getTime() - new Date(messages[i-1].dateCreated).getTime()) / 1000 > UPDATE_THRESHOLD * 60 && messages[i-1].from === message.from
        }))
    }
    return refactorMessages
  }

  getExtraFields(val: IMessage[]) {
    const newMessages = [...val].map(x => ({
      ...x,
      likes: x.reactions.filter(y => y.reaction === ReactionType.LIKE).map(z => z.user).sort((x,_) => x.id === this.user.id ? -1 : 1),
      super: x.reactions.filter(y => y.reaction === ReactionType.SUPER).map(z => z.user).sort((x,_) => x.id === this.user.id ? -1 : 1),
      liked:  x.reactions.filter(y => y.reaction === ReactionType.LIKE).find(z => z.user.id === this.user.id) ? true : false,
      supered:  x.reactions.filter(y => y.reaction === ReactionType.SUPER).find(z => z.user.id === this.user.id) ? true : false,
    })).slice().reverse();
    return newMessages;
  }

  trackByIndex(index, item){
    return item.id; 
 }

  threadMessage(message: IMessage) {
    this.contentThreadModal.openThreadMessagesModal(message);
  }

  onScroll($event: any) {
    if (this.disabled) { return; }
    const elem: HTMLElement = $event.srcElement;
    if (elem.scrollTop < 1) { elem.scrollTo(0, 1); }
    if (this.isLoading) { return; }
    if (elem.scrollTop < 50) this.fetchMore.emit()
  }

  reactionAction = (message: IMessage, index: number) => this.toggleReaction.emit({message, index})

  deleteAction = (message: IMessage) => this.deleteMessage.emit({message})

  styleObject = (): Object => ({maxWidth: `${this.dataService.config.MAX_WIDTH_MESSAGES}`})

  ngOnDestroy(): void {
    this._unsubscribeAll.next();
    this._unsubscribeAll.complete();
  }
}

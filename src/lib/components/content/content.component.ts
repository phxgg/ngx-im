import { Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AngularDataContext } from '@themost/angular';
import { Subject } from 'rxjs';
import { first, takeUntil } from 'rxjs/operators';
import { IMember } from '../../interfaces/IMember';
import { IMessage } from '../../interfaces/IMessage';
import { IReaction, IReactionPost } from '../../interfaces/IReaction';
import { DataService } from '../../services/data.service';
import { IAccount } from '../../interfaces/IAccount';

const PAGE = 20;
@Component({
  selector: 'im-content',
  templateUrl: './content.component.html',
  styleUrls: ['./content.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ContentComponent implements OnInit, OnDestroy {

  loadingChannel: boolean = false;
  loadingMessages: boolean = false;
  loadingInitMessages: boolean = false;
  index: number = 0;
  disabled: boolean = false;
  selectedId: number;
  messages: IMessage[];
  user: IMember;
  total: number;
  private _unsubscribeAll: Subject<any>;
  
  constructor( private activatedRoute: ActivatedRoute, public dataService: DataService, private context: AngularDataContext) { 
    this._unsubscribeAll = new Subject();
  }

  ngOnInit(): void {
    this.dataService.
    _account$
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe(res => {
        const data: IAccount = {...res};
        const { imAccount } = data;
        this.user = imAccount;
      });

    this.activatedRoute
      .params
        .pipe(takeUntil(this._unsubscribeAll))
        .subscribe(paramsId => {
          const selectedId = paramsId.id;
          if (selectedId) {
            this.index = 0;
            this.selectedId = parseInt(selectedId);
            this.fetchChannelMembers()
            this.fetchChannelMessages()
          };
        });
    this.dataService.
      _messages$
        .pipe(takeUntil(this._unsubscribeAll))
        .subscribe(val => {
          this.messages = val || [];
        });

    this.dataService.
      _messagesCountChannel$
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe(res => {
        this.total = res;
      });
  }

  fetchChannelMembers() {
    this.dataService.
    _channels$
      .pipe(takeUntil(this._unsubscribeAll), first(x => x && x.length > 0))
      .subscribe(val => {
        this.loadingChannel = true;
        const channel = (val || []).find(x => x.id === this.selectedId);
        if(!channel) this.loadingChannel = false; 
        else {
          this.context
          .model(`MessagingChannels/${channel.id}/members`)
          .getItems()
          .then(result => {
            this.dataService.selectedChannel = {
              ...channel,
              members: result || []
            };
          })
          .catch(error => this.dataService.error = error.message)
          .finally(() => this.loadingChannel = false);
        }
      })
  }

  fetchChannelMessages() {
    if(this.index === 0) this.loadingInitMessages = true;
    this.loadingMessages = true;
    this.context
      .model(`MessagingChannels/${this.selectedId}/messages`)
      .where("isReply")
      .equal(0)
      .expand("createdBy", "sender", "reactions","reactions($expand=user)")
      .skip(PAGE * this.index)
      .take(PAGE)
      .orderByDescending("dateCreated")
      .getList()
      .then(results => {
        const {value, total} = results;
        if(this.index === 0) {
          this.dataService.messagesCountChannel = total;
          this.dataService.messages = [];
        }
        this.dataService.messages = [...this.messages, ...value]
        this.disabled = value.length < PAGE;
        this.index++;
      })
      .catch(error => {
        this.disabled = true;
        this.dataService.error = error.message
      })
      .finally(() => {
        this.loadingInitMessages = false;
        this.loadingMessages = false
      });
  }

  fetchMore() {
    if(!this.disabled) this.fetchChannelMessages()
  }

  reactionAction(event: {message: IMessage, index: number}) {
    const { message, index } = event;
    const reaction: IReactionPost = {
      reaction: index,
      instantMessage: message.id
    }
    const tempDisabled = this.disabled;
    this.disabled = true;
    this.context
    .model(`MessagingChannels/${this.selectedId}/toggleReaction`)
    .save(reaction)
    .then((results: IReaction) => {
        const newMessage: IMessage = 
          results ? {
            ...message,
            reactions: [
              ...message.reactions, 
              {
              ...results,
              user: this.user
              }
            ].sort((x,y) => x.id - y.id)
          } : {
            ...message,
            reactions: message.reactions.filter(x => !(x.reaction === index && x.user.id === this.user.id))
          }
        const newMessages = this.messages.map(x => x.id === newMessage.id ? newMessage : x);
        this.dataService.messages = [...newMessages];
    })
    .catch(error => this.dataService.error = error.message)
    .finally(() => this.disabled = tempDisabled);
  }

  deleteMessage(event: {message: IMessage}) {
    const { message } = event;
    const tempDisabled = this.disabled;
    this.disabled = true;
    this.context
    .model(`MessagingChannels/${this.selectedId}/removeMessage`)
    .save(message)
    .then((results: {item: IMessage; children: IMessage}) => {
      const { item } = results;
      this.dataService.messages = [...this.messages].filter(x => x.id !== item.id);
      this.dataService.messagesCountChannel = this.total - 1;
    })
    .finally(() => this.disabled = tempDisabled);
  }

  ngOnDestroy(): void {
    this._unsubscribeAll.next();
    this._unsubscribeAll.complete();
  }
}

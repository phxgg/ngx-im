import { TestBed } from '@angular/core/testing';

import { NgxImService } from './ngx-im.service';

describe('NgxImService', () => {
  let service: NgxImService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(NgxImService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

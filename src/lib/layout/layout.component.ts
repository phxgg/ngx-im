import { Component, OnDestroy, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { AngularDataContext } from '@themost/angular';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { AddChannelComponent } from '../components/modals/add-channel/add-channel.component';
import { IChannel } from '../interfaces/IChannel';
import { DataService } from '../services/data.service';
import { IAccount } from '../interfaces/IAccount';
@Component({
  selector: 'im-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class LayoutComponent implements OnInit, OnDestroy {
  @ViewChild(AddChannelComponent) addChannelModal: AddChannelComponent;
  channels: IChannel[] = [];
  loading: boolean;
  private _unsubscribeAll: Subject<any>;
  
  constructor(private context: AngularDataContext, public dataService: DataService) {
    this._unsubscribeAll = new Subject();
  }

  ngOnInit() {
    this.context.model('users/me')
        .asQueryable()
        .expand('imAccount', "groups")
        .getItem().then(res => {
          const user: IAccount = {...res};
          this.dataService.account = {...user};
        })

    this.dataService.
        _channels$
          .pipe(takeUntil(this._unsubscribeAll))
          .subscribe(val => this.channels = val)
    this.fetchChannels();
  };

  fetchChannels() {
    this.loading = true;
    this.context
        .model("MessagingChannels")
        .select("id", "alternateName", "maintainer", "dateCreated", "headline", "thumbnailUrl", "isDirect", "isDisabled", "type")
        .where('isDisabled')
        .notEqual(true)
        .orderByDescending("dateCreated")
        .expand('keywords')
        .getItems()
        .then(results => this.dataService.channels = results)
        .catch(error => this.dataService.error = error.message)
        .finally(() => this.loading = false);
  }

  openAddChannel(type: 'channel' | 'direct') {
    this.addChannelModal.openAddChannelModal(type)
  }

  newChannel(channel: IChannel) {
    this.dataService.channels = [
      ...this.channels, 
      { ...channel }
    ]
  }

  styleObject = (): Object => ({minHeight: `${this.dataService.config.MIN_HEIGHT_IM}`, maxHeight: `${this.dataService.config.MAX_HEIGHT_IM}`})

  ngOnDestroy(): void {
    this._unsubscribeAll.next();
    this._unsubscribeAll.complete();
  }
}
